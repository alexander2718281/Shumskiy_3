package ru.mail.alexander_kurlovich3;

public class CreditCard {
    private int accountNumber;
    private int balance;

    public CreditCard(int accountNumber, int balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void topUpBalance (int summ){
        balance += summ;
        System.out.println("Balance increased by " + summ + " dollars");
    }

    public void withdrowMiney(int summ){
        if (summ <= balance){
            balance -= summ;
            System.out.println(summ + " dollars removed from balance");
        }
        else {
            System.out.println("There are not enough funds on the account");
        }
    }

    public void printInfo(){
        System.out.println("Account: " + getAccountNumber());
        System.out.println("Balance: "+ getBalance());
    }
}
